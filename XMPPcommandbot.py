#!/usr/bin/python3

import configparser, getpass, logging, re, subprocess, sleekxmpp, ssl, sys
from optparse import OptionParser
from time import sleep

class commandbot(sleekxmpp.ClientXMPP):
    def __init__(self, jid, password):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.message)

        self.allowed_functions = ['exit', 'exe', 'join']


    def start(self, event):
        self.get_roster()
        self.send_presence()
        self.nick = str(self.boundjid).split('@')[0]


    def group_message(self, msg):
        if self.nick not in str(msg['from']):
            pass 


    def message(self, msg):
        if 'groupchat_message' in (msg['type']):
            return None

        if self.nick not in str(msg['from']):   
            if msg['type'] in ('normal', 'chat'):

                if (str(config['security']['restrict_users']).lower() == 'true' and str(msg['from']).split('@')[0] in config['security']['whitelist_users']) or str(config['security']['restrict_users']).lower() == 'false':
                    self.process_message(msg)

                else:
                    self.send_message(mto=msg['from'], mbody="Operation not permitted")



    def join(self, room):
        self.plugin['xep_0045'].joinMUC(room, self.nick, wait=True)


    def process_message(self, msg):
        matches = re.match("(?P<function>[a-zA-Z0-9]+):{(?P<argument>.+)}", msg['body'])
        if matches:
            m = matches.groupdict()

            logging.debug("Received function: " + m['function'] + " with argument " + m['argument'])
            
            if m['function'] in self.allowed_functions:
                if m['function'] == 'exit' and m['argument'] == 'now':
                    exit()
                
                elif m['function'] == 'join':
                    self.join(m['argument'])

                elif m['function'] == 'exe':
                    #Adjust this to account for locking down beyond the initial command. 
                    if  (str(config['security']['restrict_commands']).lower() == 'true' and str(m['argument']).split(' ')[0] in config['security']['whitelist_commands']) or str(config['security']['restrict_commands']).lower() == 'false':
                        self.send_message(mto=msg['from'], mbody="Executing command: " + m['argument'])

                        with subprocess.Popen(m['argument'].split(), stdout=subprocess.PIPE, bufsize=1, universal_newlines=True) as process:
                            for line in process.stdout:
                                self.send_message(mto=msg['from'], mbody=line.strip(), mtype='chat')

                        self.send_message(mto=msg['from'], mbody="complete.", mtype='chat')
                    
                    else:
                        self.send_message(mto=msg['from'], mbody=m['argument'] + " is not contained in the whitelist_commands parameter in " + __file__.replace(".py", ".ini"))

            else:
                self.send_message(mto=msg['from'], mbody=m['function'] + " is unrecognized")
        

##############
def loadconfig():
    #implement error checking and defaults in case .ini is not found
    config = configparser.ConfigParser()
    config.read(__file__.replace(".py", ".ini"))
    return config


if __name__ == '__main__':
    config = loadconfig()
    parser = OptionParser()
    
    parser.add_option("-d", "--debug",
        help="Set debuging",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.INFO)

    if 'jid' not in config['connection'] or config['connection']['jid'] == '':
        parser.add_option("-j", "--jid",
            dest="jid",
            help="JID to use")
    else:
        parser.add_option("-j", "--jid",
            dest="jid",
            default=config['connection']['jid'],
            help="JID to use")

    if 'password' not in config['connection'] or config['connection']['password'] == '':
        parser.add_option("-p", "--password",
            dest="password",
            help="password to use")
    else:
        parser.add_option("-p", "--password",
            dest="password",
            default=config['connection']['password'],
            help="password to use")

    opts, args = parser.parse_args()

    logging.basicConfig(level=opts.loglevel, format='%(levelname)-8s %(message)s')

    if opts.jid is None:
            opts.jid = input("Username: ")

    if opts.password is None:
            opts.password = getpass.getpass("Password: ")

    
    xmpp = commandbot(opts.jid, opts.password)
    #xmpp.register_plugin('xep_0030')
    xmpp.register_plugin('xep_0004')
    #xmpp.register_plugin('xep_0045')
    xmpp.register_plugin('xep_0060')
    xmpp.register_plugin('xep_0199')

    if xmpp.connect():
        xmpp.process(block=True)
    else:
        print("Unable to connect")
